function show(url) {
	with(new XMLHttpRequest) {
		onload=function() {
			document.body.insertAdjacentHTML("beforeend",responseText)
		};
		open("GET",url,false);
		send();
	}
}
function showOptions(){
	var c1 = document.getElementById("option1").value;
	var c2 = document.getElementById("option2").value;
	show(c1);
	show(c2);
}

function changeButton()
{
	var elem = document.getElementById("button");
	elem.value = "Clean";
	elem.onclick = function(){ window.location.reload(); } ;;
}
