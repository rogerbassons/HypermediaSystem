function show(url) {
	with(new XMLHttpRequest) {
		onload=function() {
			document.body.insertAdjacentHTML("beforeend",responseText)
		};
		open("GET",url,false);
		send();
	}
}
